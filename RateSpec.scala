import org.scalactic.{Equality, TolerantNumerics, TypeCheckedTripleEquals}
import org.scalatest.{Matchers, WordSpec}

class RateSpec extends WordSpec with Matchers with TypeCheckedTripleEquals {

  implicit val doubleEquality: Equality[Double] =
    TolerantNumerics.tolerantDoubleEquality(0.0001)

  "Rate.monthlyRate" should {
    "return a fixed rate for a FixedRate" in {
      Rates.monthlyRate(FixedRate(0.04), 0) should ===(0.04 / 12)
      Rates.monthlyRate(FixedRate(0.04), 10) should ===(0.04 / 12)
    }

    val variableRates = VariablesRate(List(
      VariableRate(1, 0.1),
      VariableRate(2, 0.2)))
    "return the nth rate for VariableRate" in {
      Rates.monthlyRate(variableRates, 0) should ===(0.1)
      Rates.monthlyRate(variableRates, 1) should ===(0.2)
    }

    "roll over from the first rate if n > length" in {
      Rates.monthlyRate(variableRates, 2) should ===(0.1)
      Rates.monthlyRate(variableRates, 3) should ===(0.2)
      Rates.monthlyRate(variableRates, 4) should ===(0.1)
    }
  }
}