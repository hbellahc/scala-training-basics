# Developpement d'une calculatrice de retraite 




# Classe de tests : 

```scala 
import org.scalactic.{Equality, TolerantNumerics, TypeCheckedTripleEquals}
import org.scalatest.{Matchers, WordSpec}

class RetCalcSpec extends WordSpec with Matchers with TypeCheckedTripleEquals {

  implicit val doubleEquality: Equality[Double] = TolerantNumerics.tolerantDoubleEquality(0.0001)
  "RetCalc.futureCapital" should {
     // assertion 1
     // assertion 2     
  }

  // assertion 3    

  // assertion 4

 
}
``` 
## Assertion 1 : Test de la méthode futureCapital (phase commulative)
```scala 
 
  "calculate the amount of savings I will have in n months" in {
    val actual = RetCalc.futureCapital(
      interestRate = 0.04 / 12, nbOfMonths = 25 * 12,
      netIncome = 3000, currentExpenses = 2000,
      initialCapital = 10000)
    val expected = 541267.1990
    actual should ===(expected)
  }
  
  ```

  ## Assertion 2 : Test de la méthode futureCapital (phase decommulative)
```scala 
 "RetCalc.futureCapital" should {
    "calculate how much savings will be left after having taken a pension for n months" in {
      val actual = RetCalc.futureCapital(
        interestRate = 0.04 / 12, nbOfMonths = 40 * 12,
        netIncome = 0, currentExpenses = 2000, initialCapital =
          541267.1990)
      val expected = 309867.53176
      actual should ===(expected)
    }
  }
  
  ```

  ## Assertion 3 : Test de la méthode simulatePlan (simulation d'un plan de retraite)
```scala 
 "RetCalc.simulatePlan" should {
    "calculate the capital at retirement and the capital after death" in {
      val (capitalAtRetirement, capitalAfterDeath) =
        RetCalc.simulatePlan(
          interestRate = 0.04 / 12,
          nbOfMonthsSaving = 25 * 12, nbOfMonthsInRetirement = 40 * 12,
          netIncome = 3000, currentExpenses = 2000,
          initialCapital = 10000)
      capitalAtRetirement should ===(541267.1990)
      capitalAfterDeath should ===(309867.5316)
    }
  }
  ```
  ## Assertion 4 : Test de la méthode nbOfMonthsSaving (Quand est-ce qu'on peut partir à la retraite)
  ```scala 
  "RetCalc.nbOfMonthsSaving" should {
    "calculate how long I need to save before I can retire" in {
      val actual = RetCalc.nbOfMonthsSaving(
        interestRate = 0.04 / 12, nbOfMonthsInRetirement = 40 * 12,
        netIncome = 3000, currentExpenses = 2000, initialCapital = 10000)
      val expected = 23 * 12 + 1
      actual should ===(expected)
    }
  }
  ```

## Assertion pour tester la nouvelle fonction du calcul du capital  

```scala 
  "RetCalc.futureCapital2" should {
    "calculate the amount of savings I will have in n months" in {
      val actual = RetCalc.futureCapital2(FixedRate(0.04),
        nbOfMonths = 25 * 12, netIncome = 3000,
        currentExpenses = 2000, initialCapital = 10000)
      val expected = 541267.1990
      actual should ===(expected)
    }

    "calculate how much savings will be left after having taken a pension for n months" in {
      val actual = RetCalc.futureCapital2(FixedRate(0.04),
        nbOfMonths = 40 * 12, netIncome = 0, currentExpenses = 2000,
        initialCapital = 541267.198962)
      val expected = 309867.5316
      actual should ===(expected)
    }
  }
``` 
## Signature de la fonction 
```scala 
  def futureCapital2(rate: Rate, nbOfMonths: Int,
                     netIncome: Int, currentExpenses: Int,
                     initialCapital: Double): Double = {

  // TODO 
   
  }  
  ```
## Implementation de l'ancienne fonction de calcul pour vous aider
```scala 
     def futureCapital(interestRate: Double, nbOfMonths: Int,
                    netIncome: Int, currentExpenses: Int,
                    initialCapital: Double): Double = {


        (1 to nbOfMonths).foldLeft(initialCapital)((cumul, month) => cumul * (1 + interestRate) + (netIncome - currentExpenses))
  }

  ```
